from gevent.pywsgi import WSGIServer
from app.server import app

WSGIServer(('::', 8180), app).serve_forever()
