import logging

import flask
import pprint

### flask app
app = flask.Flask(__name__)
app.config.from_object('config.Config')
app.config.from_pyfile(app.config['SITE_CONFIG_PATH'] + '/config.py')

logging.basicConfig(format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")
logging.getLogger().setLevel(logging.DEBUG)
#app.logger.setLevel(logging.DEBUG)

log = logging.getLogger(__name__)
log.debug('global: __name__ {}'.format( __name__ ))
log.debug('global: app.config \n{}\n\n'.format( pprint.pformat(app.config) ))

@app.errorhandler(400)
def bad_request(e):
    return str(e), 400

@app.errorhandler(500)
def internal_server_error(e):
    return str(e), 500


from . import views
