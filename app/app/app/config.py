from .server import app

import pathlib

#import flask
import werkzeug
import pprint

import yaml
import deepmerge

class AppClientConfig(object):
    def __init__(self,
            clientHost):
        self.clientHost = clientHost
        self.clientConfig = self._getClientConfig(listStrategy = 'override')
        self._getOSClass()
        self._getDeployMethod()
        self.repoConfig = self._getRepoConfig()
        self.softwareConfig = self._getSoftwareConfig()
        self.storageConfig = self._getStorageConfig()

    @staticmethod
    def firstFound(fs):
        for f in fs:
            if pathlib.Path(f).is_file():
                return f
        return False
            
    def _getClientConfig(self, **kwargs):
        defaultConfigFile = app.config['GLOBAL_CONFIG_PATH'] + '/defaults/init.yml' 
        siteConfigFile = app.config['SITE_CONFIG_PATH'] + '/defaults/init.yml' 
        clientConfigFile = app.config['SITE_CONFIG_PATH'] + '/hosts/' + self.clientHost + '.yml' 

        return self._mergeConfigFiles(defaultConfigFile, siteConfigFile, clientConfigFile, **kwargs)

    def _getOSClass(self, **kwargs):
        OSClassesConfigFile = app.config['GLOBAL_CONFIG_PATH'] + '/OSClasses/init.yml'
        OSClasses = self._getConfigFromFile(OSClassesConfigFile)

        OS = self.clientConfig.get('OS')

        for k,v in OSClasses.items():
            if OS.get('name') in v.get('OSs'):
                self._mergeConfig(self.clientConfig, {'OS': { 'class': k } })

    def _getDeployMethod(self, **kwargs):
        deployMethodsConfigFile = app.config['GLOBAL_CONFIG_PATH'] + '/deployMethods/init.yml'
        deployMethods = self._getConfigFromFile(deployMethodsConfigFile)

        OS = self.clientConfig.get('OS')                

        for k,v in deployMethods.items():
            if OS.get('name') in v.get('OSs'):
                self._mergeConfig(self.clientConfig, {'OS': { 'deployMethod': k } })


    def _getRepoConfig(self, **kwargs):
        basePaths = [ 
            app.config['SITE_CONFIG_PATH'] + '/repo',
            app.config['GLOBAL_CONFIG_PATH'] + '/repo',
        ]

        OS = self.clientConfig.get('OS')
        repoClasses = [ 'init' ] + self.clientConfig.get('repo').get('classes', [])
        app.logger.debug('OS: {}\n'.format( pprint.pformat(OS)))
        app.logger.debug('repoClasses: {}\n'.format( pprint.pformat(repoClasses)))

        repoConfig = {}
        repoConfFiles=[]
        for repoClass in repoClasses:

            searchOrder = [
                'OS.name/' + str(OS.get('name')) + '/OS.ver/' + str(OS.get('ver')) + '/',
                #'OS.name/' + str(OS.get('name')) + '/',
                #'OS.class/' + str(OS.get('class')) + '/OS.ver/' + str(OS.get('ver')) + '/',
                #'OS.class/' + str(OS.get('class')) + '/',
                #'',
            ]

            repoConfFilesClass = []
            for searchItem in searchOrder:
                repoConfFilesClass.append(searchItem + repoClass + '.yml')
            
            searchList = []
            for basePath in basePaths:
                for repoConfFileClass in repoConfFilesClass:
                    searchList.append(basePath + '/' + repoConfFileClass)

            app.logger.debug('searchList: {}\n'.format( pprint.pformat(searchList)))

            repoConfFile = self.firstFound(searchList)
            app.logger.debug('repoConfFile: {}\n'.format(repoConfFile))

            if repoConfFile:
                repoConfFiles.append(repoConfFile)

        app.logger.debug('repoConfFiles: {}\n'.format(repoConfFiles))

        repoConfig = self._mergeConfigFiles(*repoConfFiles, **kwargs)

        return repoConfig


    def _getSoftwareConfig(self, **kwargs):
        basePaths = [ 
            app.config['SITE_CONFIG_PATH'] + '/software',
            app.config['GLOBAL_CONFIG_PATH'] + '/software',
        ]
        
        OS = self.clientConfig.get('OS')
        softwareClasses = [ 'init' ] + self.clientConfig.get('software').get('classes', [])
        app.logger.debug('OS: {}\n'.format( pprint.pformat(OS)))
        app.logger.debug('softwareClasses: {}\n'.format( pprint.pformat(softwareClasses)))

        softwareConfig = {}
        softwareConfFiles=[]
        for softwareClass in softwareClasses:

            searchOrder = [
                'OS.name/' + str(OS.get('name')) + '/OS.ver/' + str(OS.get('ver')) + '/',
                'OS.name/' + str(OS.get('name')) + '/',
                'OS.class/' + str(OS.get('class')) + '/OS.ver/' + str(OS.get('ver')) + '/',
                'OS.class/' + str(OS.get('class')) + '/',
                '',
            ]
            
            softwareConfFilesClass = []
            for searchItem in searchOrder:
                softwareConfFilesClass.append(searchItem + softwareClass + '.yml')
           

            searchList = []
            for basePath in basePaths:
                for softwareConfFileClass in softwareConfFilesClass:
                    searchList.append(basePath + '/' + softwareConfFileClass)

            app.logger.debug('searchList: {}\n'.format( pprint.pformat(searchList)))
        
            softwareConfFile = self.firstFound(searchList)
            app.logger.debug('softwareConfFile: {}\n'.format(softwareConfFile))

            if softwareConfFile:
                softwareConfFiles.append(softwareConfFile)

        app.logger.debug('softwareConfFiles: {}\n'.format(softwareConfFiles))

        softwareConfig = self._mergeConfigFiles(*softwareConfFiles, **kwargs)

        return softwareConfig

    def _getStorageConfig(self, **kwargs):
        basePaths = [ 
            app.config['SITE_CONFIG_PATH'] + '/storage',
            app.config['GLOBAL_CONFIG_PATH'] + '/storage',
        ]
        
        OS = self.clientConfig.get('OS')
        storageClass = self.clientConfig.get('storage').get('class')
        app.logger.debug('OS: {}\n'.format( pprint.pformat(OS)))
        app.logger.debug('storageClass: {}\n'.format( pprint.pformat(storageClass)))

        storageConfig = {}
        storageConfFiles=[]

        searchOrder = [
            'OS.deployMethod/' + str(OS.get('deployMethod')) + '/OS.name/' + str(OS.get('name')) + '/OS.ver/' + str(OS.get('ver')) + '/',
            'OS.deployMethod/' + str(OS.get('deployMethod')) + '/OS.name/' + str(OS.get('name')) + '/',
            'OS.deployMethod/' + str(OS.get('deployMethod')) + '/OS.class/' + str(OS.get('class')) + '/OS.ver/' + str(OS.get('ver')) + '/',
            'OS.deployMethod/' + str(OS.get('deployMethod')) + '/OS.class/' + str(OS.get('class')) + '/',
            'OS.deployMethod/' + str(OS.get('deployMethod')) + '/',
        ]
            
        storageConfFilesClass = []
        for searchItem in searchOrder:
            storageConfFilesClass.append(searchItem + storageClass + '.yml')

        searchList = []
        for basePath in basePaths:
            for storageConfFileClass in storageConfFilesClass:
                searchList.append(basePath + '/' + storageConfFileClass)

        app.logger.debug('searchList: {}\n'.format( pprint.pformat(searchList)))
    
        storageConfFile = self.firstFound(searchList)
        app.logger.debug('storageConfFile: {}\n'.format(storageConfFile))

        if storageConfFile:
            storageConfFiles.append(storageConfFile)

        app.logger.debug('storageConfFiles: {}\n'.format(storageConfFiles))

        storageConfig = self._mergeConfigFiles(*storageConfFiles, **kwargs)

        return storageConfig

    
    def _mergeConfigFiles(self, *configFiles, **kwargs):
        configs=[]
        for configFile in configFiles:
           configs.append(self._getConfigFromFile(configFile))
        return self._mergeConfig(*configs, **kwargs)


    def _mergeConfig(self, *configs, **kwargs):
        # possible values: append override prepend
        listStrategy = kwargs.get('listStrategy', 'append')
        # possible values: merge override
        dictStrategy = kwargs.get('dictStrategy', 'merge')
        # possible values: override use_existing
        fallbackStrategy = kwargs.get('fallbackStrategy', 'override')
        # possible values: override override_if_not_empty use_existing
        conflictStrategy = kwargs.get('conflictStrategy', 'override')
         
        app.logger.debug('configs {}'.format( pprint.pformat(configs) ))

        configMerger = deepmerge.Merger(
            [
                (list, [listStrategy]),
                (dict, [dictStrategy])
            ],
            [fallbackStrategy],
            [conflictStrategy]
        )
        config0 = configs[0]
        app.logger.debug('config0:\n{}\n\n'.format( pprint.pformat(config0) ))
        for config in configs[1:]:
            configMerger.merge( config0, config )
            app.logger.debug('config:\n{}\n\n'.format( pprint.pformat(config) ))
            app.logger.debug('merged config0: \n{}\n\n'.format( pprint.pformat(config0) ))
    
        return config0

    def _getConfigFromFile(self, configFile):
        try:
            with open(configFile, "r") as f:
                return yaml.safe_load(f)
        except IOError as e:
            errno, strerror = e.args
            #flask.abort (400, description = 'configFile {}: I/O error({}): \n{}\n\n'.format( configFile, errno, pprint.pformat(strerror) ))
            raise werkzeug.exceptions.BadRequest('configFile {}: I/O error({}): \n{}\n\n'.format( configFile, errno, pprint.pformat(strerror) ) )
        except yaml.YAMLError as e:
            #flask.abort (400, description = 'configFile {}: Invalid YAML: \n{}\n\n'.format( configFile, pprint.pformat(str(e)) )) 
            raise werkzeug.exceptions.InternalServerError(description = 'configFile {}: Invalid YAML: \n{}\n\n'.format( configFile, pprint.pformat(str(e)) ) )
    
        except Exception as e:
            #flask.abort (400, description = 'configFile {}: could not be read with error: \n{}\n\n'.format( configFile, pprint.pformat(str(e)) ))
            raise werkzeug.exceptions.InternalServerError(description = 'configFile {}: could not be read with error: \n{}\n\n'.format( configFile, pprint.pformat(str(e)) ) )
    
    
    #def setClientConfig(clientHost):
