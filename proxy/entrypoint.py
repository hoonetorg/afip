#!/usr/bin/env python3
import os, subprocess, jinja2

def main():
    jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader('/haproxy'))
    template = jinja_env.get_template('haproxy.cfg.j2')
    template.stream(env=os.environ).dump('/usr/local/etc/haproxy/haproxy.cfg')
    print('running subprocess now')
    subprocess.run(['haproxy', '-W', '-db', '-f', '/usr/local/etc/haproxy/haproxy.cfg'])

if __name__ == '__main__':
    main()
