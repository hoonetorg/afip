## AFIP mainly a wrapper around kickstart and preseed installations.

Disclaimer regarding the name: The app has no name! AFIP is just used to have a name 
for identify this app and to save it somwhere.
If the app should be renamed to ThomasApp f.e. tomorrow, then you just need 
to rename the folder where the app is saved in and 
to rename the folders where your site configurations are saved.
This is done to be able to rename the app at any time, e.g. when the used name is 
registered, a trademark or something else.

Compareble products (which mostly have much more functionality) are: FAI, Cobbler, Foreman.

This app is written in Python 3. The Flask framework is used.

It creates a boot file for iPXE (boot.ipxe), a kickstart file (ks.cfg.<some distro>) or a preseed file (preseed.<some distro>)

There are so many os deployment tools, why this app?

- Installation is done via the provided (and supported) installer of the OS
  - Installed system should be correctly installed:
    - no missing mountpoints,
    - no wrong boot configuration,
    - no malfunction after updates
- Installation via web-server and DHCP-server only:
  - DHCP defines a HTTP/HTTPS - location for the boot-file (boot.ipxe)
  - boot.ipxe is an iPXE-file
  - Server boots from iPXE-boot-rom (iso, usb, chainload from PXE or directly flashed into network card)
  - dynamically generated boot.ipxe let's:
    - server boot from hard disk, if client is configured not to be installed
    - server boot kernel+initrd from HTTP/HTTPS - location.
- Dynamically generated kickstart file (ks.cfg.<some distro>):
  - allows creating:
    - profiles for difficult %pre - scripts (used by partioning f.e.)
    - profiles for all settings in the installation (language, keyboard, bootloader, initial passwords f.e)
    - profiles for installed packages
    - profiles for used software repos(including proxy-configuration per repo)
  - from simple settings
    - in per installation-host config file or 
    - default settings file

###Installation

This app runs as a container pod. In this pod run all required containers.
Currently this works out of the box with podman.
It is planned to createKubernetes/Helm/OpenShift deployment configs

Configure this app (config.py, default.yml, repos, partitioning schemes, post classes, hosts)
 
###BUILD
#### create appcontainer

change folder to root of this repo

```
make
```
