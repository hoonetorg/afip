#!/bin/bash
PATH_SCRIPT="$(OLDPWD="`pwd`";cd "`dirname "$0"`";echo "`pwd`";cd "$OLDPWD")"
echo "PATH_SCRIPT: \"$PATH_SCRIPT\""

NAME_APP="`echo "${PATH_SCRIPT}"| awk -F '/' '{print $(NF-1)}'`"
echo "NAME_APP: \"$NAME_APP\""

PROG_DIR="`dirname "${PATH_SCRIPT}"`"
echo "PROG_DIR: \"$PROG_DIR\""

if [ "$2" == "dev" ]; then
    DEV=true
fi

PROJECT="$1"
[ -z "$PROJECT" ] && export PROJECT="default"

[ -z "$APP_CFG_DIR" ] && export APP_CFG_DIR=/srv/containers/prj/${PROJECT}/${NAME_APP}/cfg
[ -d "$APP_CFG_DIR" ] || exit 1

[ -f "$APP_CFG_DIR/app.cfg" ] && source $APP_CFG_DIR/app.cfg

[ -z "$APP_DATA_DIR" ] && export APP_DATA_DIR=/srv/containers/prj/${PROJECT}/${NAME_APP}/data
[ -d "$APP_DATA_DIR" ] || exit 1

[ -z "$DNSMASQ_CFG_DIR" ] && export DNSMASQ_CFG_DIR=${APP_CFG_DIR}/dnsmasq
[ -d "$DNSMASQ_CFG_DIR" ] || exit 1

[ -z "$DNSMASQ_TFTP_DIR" ] && export DNSMASQ_TFTP_DIR=${APP_DATA_DIR}/dnsmasq/tftp
[ -d "$DNSMASQ_TFTP_DIR" ] || exit 1

[ -z "$APP_SITECFG_DIR" ] && export APP_SITECFG_DIR=${APP_CFG_DIR}/app
[ -d "$APP_SITECFG_DIR" ] || exit 1

[ -z "$APP_WORK_DIR" ] && export APP_WORK_DIR=${APP_DATA_DIR}/app/work
[ -d "$APP_WORK_DIR" ] || exit 1

[ -z "$REPO_DIR" ] && export REPO_DIR=${APP_DATA_DIR}/repo
[ -d "$REPO_DIR" ] || exit 1

[ -z "$APP_LISTEN_IP" ] && export APP_LISTEN_IP=""

[ -n "$APP_LISTEN_IP" ] && export APP_LISTEN_IP_STRING="hostIP: ${APP_LISTEN_IP}" || export APP_LISTEN_IP_STRING=""

echo "APP_LISTEN_IP: \"$APP_LISTEN_IP\""
echo "APP_LISTEN_IP_STRING: \"$APP_LISTEN_IP_STRING\""

# cleaning up
echo "removing existing pod dhcphelper-pod-${PROJECT}-pod-0"
podman pod rm -if dhcphelper-pod-${PROJECT}-pod-0
echo "removing existing pod app-pod-${PROJECT}"
podman pod rm -if app-pod-${PROJECT}
#echo "removing existing network app-${PROJECT}"
#podman network rm -f app-${PROJECT}

# network
#echo "creating new network app-${PROJECT}"
#podman network create --disable-dns --subnet=192.168.101.0/24 --gateway=192.168.101.1 app-${PROJECT}


# app pod
#    --ip=192.168.101.253 \
# is not supported when running via play kube. we try to live without it
#    --dns=::1 \

if [ "$DEV" == true ]; then
    echo "creating new pod app-pod-${PROJECT} in dev mode"
    cp ${PATH_SCRIPT}/app-dev.yaml.in ${PATH_SCRIPT}/app-${PROJECT}-dev.yaml
    echo "DNSMASQ_CFG_DIR: ${DNSMASQ_CFG_DIR}"
    sed -i -r "s;##DNSMASQ_CFG_DIR##;${DNSMASQ_CFG_DIR};g" ${PATH_SCRIPT}/app-${PROJECT}-dev.yaml
    echo "DNSMASQ_TFTP_DIR: ${DNSMASQ_TFTP_DIR}"
    sed -i -r "s;##DNSMASQ_TFTP_DIR##;${DNSMASQ_TFTP_DIR};g" ${PATH_SCRIPT}/app-${PROJECT}-dev.yaml
    echo "APP_SITECFG_DIR: ${APP_SITECFG_DIR}"
    sed -i -r "s;##APP_SITECFG_DIR##;${APP_SITECFG_DIR};g" ${PATH_SCRIPT}/app-${PROJECT}-dev.yaml
    echo "APP_WORK_DIR: ${APP_WORK_DIR}"
    sed -i -r "s;##APP_WORK_DIR##;${APP_WORK_DIR};g" ${PATH_SCRIPT}/app-${PROJECT}-dev.yaml
    echo "REPO_DIR: ${REPO_DIR}"
    sed -i -r "s;##REPO_DIR##;${REPO_DIR};g" ${PATH_SCRIPT}/app-${PROJECT}-dev.yaml
    echo "PROG_DIR: ${PROG_DIR}"
    sed -i -r "s;##PROG_DIR##;${PROG_DIR};g" ${PATH_SCRIPT}/app-${PROJECT}-dev.yaml
    echo "APP_LISTEN_IP_STRING: ${APP_LISTEN_IP_STRING}"
    sed -i -r "s;##APP_LISTEN_IP_STRING##;${APP_LISTEN_IP_STRING};g" ${PATH_SCRIPT}/app-${PROJECT}-dev.yaml
    echo "PROJECT: ${PROJECT}"
    sed -i -r "s;##PROJECT##;${PROJECT};g" ${PATH_SCRIPT}/app-${PROJECT}-dev.yaml
    #    --network=app \
    podman play kube \
        ${PATH_SCRIPT}/app-${PROJECT}-dev.yaml
    rm ${PATH_SCRIPT}/app-${PROJECT}-dev.yaml
else
    echo "creating new pod app-pod-${PROJECT} in prod mode"
    cp ${PATH_SCRIPT}/app-prod.yaml.in ${PATH_SCRIPT}/app-${PROJECT}-prod.yaml
    echo "DNSMASQ_CFG_DIR: ${DNSMASQ_CFG_DIR}"
    sed -i -r "s;##DNSMASQ_CFG_DIR##;${DNSMASQ_CFG_DIR};g" ${PATH_SCRIPT}/app-${PROJECT}-prod.yaml
    echo "DNSMASQ_TFTP_DIR: ${DNSMASQ_TFTP_DIR}"
    sed -i -r "s;##DNSMASQ_TFTP_DIR##;${DNSMASQ_TFTP_DIR};g" ${PATH_SCRIPT}/app-${PROJECT}-prod.yaml
    echo "APP_SITECFG_DIR: ${APP_SITECFG_DIR}"
    sed -i -r "s;##APP_SITECFG_DIR##;${APP_SITECFG_DIR};g" ${PATH_SCRIPT}/app-${PROJECT}-prod.yaml
    echo "APP_WORK_DIR: ${APP_WORK_DIR}"
    sed -i -r "s;##APP_WORK_DIR##;${APP_WORK_DIR};g" ${PATH_SCRIPT}/app-${PROJECT}-prod.yaml
    echo "REPO_DIR: ${REPO_DIR}"
    sed -i -r "s;##REPO_DIR##;${REPO_DIR};g" ${PATH_SCRIPT}/app-${PROJECT}-prod.yaml
    echo "APP_LISTEN_IP_STRING: ${APP_LISTEN_IP_STRING}"
    sed -i -r "s;##APP_LISTEN_IP_STRING##;${APP_LISTEN_IP_STRING};g" ${PATH_SCRIPT}/app-${PROJECT}-prod.yaml
    echo "PROJECT: ${PROJECT}"
    sed -i -r "s;##PROJECT##;${PROJECT};g" ${PATH_SCRIPT}/app-${PROJECT}-prod.yaml
    #    --network=app \
    podman play kube \
        ${PATH_SCRIPT}/app-${PROJECT}-prod.yaml
    rm ${PATH_SCRIPT}/app-${PROJECT}-prod.yaml
fi


# dhcphelper pod
#APP_POD_IP="$(podman inspect --type container `podman pod inspect app-pod-${PROJECT} --format "{{.InfraContainerID}}"` --format "{{.NetworkSettings.Networks.app.IPAddress}}")"
APP_POD_IP="$(podman inspect --type container `podman pod inspect app-pod-${PROJECT} --format "{{.InfraContainerID}}"` --format "{{.NetworkSettings.IPAddress}}")"
echo "app-pod-${PROJECT} IP: ${APP_POD_IP}"

cp ${PATH_SCRIPT}/dhcphelper.yaml.in ${PATH_SCRIPT}/dhcphelper-${PROJECT}.yaml
sed -i -r "s/##APP_POD_IP##/${APP_POD_IP}/g" ${PATH_SCRIPT}/dhcphelper-${PROJECT}.yaml
sed -i -r "s;##PROJECT##;${PROJECT};g" ${PATH_SCRIPT}/dhcphelper-${PROJECT}.yaml

echo "creating new pod dhcphelper-pod-${PROJECT}-pod-0"
podman play kube \
    ${PATH_SCRIPT}/dhcphelper-${PROJECT}.yaml

rm ${PATH_SCRIPT}/dhcphelper-${PROJECT}.yaml
